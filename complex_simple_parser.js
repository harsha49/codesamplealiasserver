/*
 * complex_simple_parser.js
 * Manipulate input json and return ordered topics within sub topics
 */
'use strict';
const FS = require('fs');
const CONFIGURATION = require('../config/config');
const CONFIG = new CONFIGURATION();
let convertComplexJson = {

  /**
  * Converting complex json to simple josn for genration of pdf from wkhtmltopdf
  * @param {string} <jsonObject> - <json Object for json>
  * @returns {Array} - <return array of objects with links of html>
  * @throws {execption} - execption
  */
  simplifyJson: function (jsonObject) {
    let convertedJson = [];
    try {
      if (!Array.isArray(jsonObject)) {
        jsonObject = [jsonObject];
      }
      jsonObject.forEach(element => {
        if (element.documentInfo != undefined || element.topicsMetadataList!=undefined || element.documentInfo.document_id!=undefined || element.documentInfo.doc_cms_id != undefined) {
          let itemJson = {
            documentInfo: element.documentInfo, releasesList: element.releasesList
            , iFramesList: element.iFramesList
          };
          let topicsMetadataListOutterLevel = element.topicsMetadataList.filter(function (x) {
            return ((x.topics.length == 0 && x.parentId == -1))
          });
          let topicsMetadataListInnerLevel = element.topicsMetadataList.filter(function (x) {
            return (!(x.topics.length == 0 && x.parentId == -1))
          });
          let finaltopicsMetadataList = [];
          for (let item of topicsMetadataListOutterLevel) {
            item[CONFIG.childItems] = [];
            finaltopicsMetadataList.push(item);
          }
          let newtopicsMetadataList = [];
          for (let item of topicsMetadataListInnerLevel) {
            item[CONFIG.childItems] = [];
            newtopicsMetadataList.push(item);
          }
          for (let newTopics of newtopicsMetadataList) {
            if (newTopics.topics.length > 0) {
              for (let item of newTopics.topics) {
                newTopics.childitems.push(newtopicsMetadataList.find(function (x) {
                  return x.id == item;
                }))
              }
            }
          }
          let duplicateNewTopicsMetadataList = JSON.parse(JSON.stringify(newtopicsMetadataList));
          newtopicsMetadataList.filter(function (x) {
            if (x.parentId != -1) {
              duplicateNewTopicsMetadataList.splice(duplicateNewTopicsMetadataList.indexOf(duplicateNewTopicsMetadataList.find(function (x1) {
                return x1.id == x.id;
              })), 1);
            }
          })
          let metadataList = [];
          for (let item of finaltopicsMetadataList) {
            metadataList.push(item);
          }
          for (let item of duplicateNewTopicsMetadataList) {
            metadataList.push(item);
          }
          itemJson[CONFIG.metadataList] = metadataList;

          convertedJson.push(itemJson);
        }
      });

    }
    catch (exception) {
      throw exception;
    }
    return convertedJson;
  }
}
module.exports = convertComplexJson;
