/*
 * pdfConvertor.js
 * Used for creating pdf from object return by complex_simple_parser.js  
 */
'use strict';
const express = require('express');
var app = express();
var server;
const CONFIGARATION = require('../config/config.js');
const JSON_CONVERTOR = require('./complex_simple_parser.js');
const WKHTMLTOPDF = require('wkhtmltopdfarray');
const MERGE = require('easy-pdf-merge');
const PATH = require("path");
const FS = require('fs');
let config = new CONFIGARATION();
let arrayOfLinks = [];
let mergePdfNames = "";
let list = "<ul class=\"toc_list\">";
let footerHtml = "";
let outputFolder="";

function alias(arrayOfLinks) {
    return new Promise(function (resolve, reject) {
        app.get('/*', function (req, res) {
            console.log(req.params);
            console.log('Redirecting to: ' + arrayOfLinks[+(req.params[0])]);
            res.redirect(arrayOfLinks[req.params[0]]);
        });
        server = app.listen(0, function () {
            console.log('Listening on port ' + server.address().port);
            resolve(1);
        });
    });
}

/**
* Converting Complex Json to Simple Json and fetching html to connvert into  PDF 
* @param {string} <jsonObject> - <json Object for json>
*@param {string} <outputPath> - <pdf output folder>
*@param {string} <outPutPdf> - <pdf file name>
*@param {string} <footer> - <footer path>
* @returns {Promise} 
* @throws {execption} - execption
*/
let genratePdf = function (jsonObject, outputPath, outPutPdf, footer) {

    return new Promise(function (resolve, reject) {
        try {
            outputFolder=outputPath;
            outputPath = PATH.join(__dirname, '../', outputPath);
            footerHtml = "file:///" + PATH.join(__dirname, '../', footer);
            createPdfDir(outputPath);
            mergePdfNames = outputPath + ",";
            const COMPLEX_JSON = JSON_CONVERTOR.simplifyJson(jsonObject);
            COMPLEX_JSON.forEach((element) => {
                arrayOfLinks = [];
                let nameOfTableOfContent = 'TableofCotent' + element.documentInfo.document_id + element.documentInfo.doc_cms_id + '.html';
                let currentDirectoryName = PATH.join(__dirname, '../', nameOfTableOfContent);
                arrayOfLinks.push("file:///" + currentDirectoryName.replace(/\\/g, '/'));
                let outputPathFolder = outputPath + "/" + outPutPdf + config.pdfExtension;
                complexJsonIteration(element);
                list = "";
                generateTOC(element.MetadataList);
                let text = "<html><head><style> body{background: #f9f9f9;} .wrapper{margin:5% auto; width:80%;} .toc_title {font-weight: 700;text-align: center;} ul{list-style-type: square;} li{ color: #124191; font-weight: bold; font-size: 24px; line-hight: 28px; padding:10px; font-family: Arial,Helvetica,sans-serif;}</style></head><body><div class='wrapper'><h1 class=\"toc_title\">Table of Contents</h1><div id=\"toc_container\"><test></div></div></body></html>";
                    let textReplace = text.replace(/<test>/g, '<ul>' + list + '</ul>');
                    let tempFile = FS.createWriteStream(nameOfTableOfContent);
                    tempFile.on('open', function () {
                        tempFile.write(textReplace);
                        alias(arrayOfLinks).then(function (x) {
                            var arrays=[];
                            arrayOfLinks.forEach((i,y) => {
                              
                                arrays.push("http://localhost:" + server.address().port + "/"+y); 
                            });
                            
                        WKHTMLTOPDF(arrays, {
                            output: outputPathFolder
                            , footerHtml: footerHtml,
                            marginBottom: "13mm",
                            marginTop: "13mm",
                            printMediaType: true,
                            logLevel: "error"
                        }, function (error) {
                            if (error != null) {
                                FS.exists(nameOfTableOfContent, function () {
                                    FS.unlink(nameOfTableOfContent, function () {
                                    });
                                })
                                FS.readdirSync(outputFolder).forEach(file => {
                                    if (file.startsWith(outPutPdf)) {
                                        FS.unlinkSync(outputFolder + "/"+file);
                                    }
                                })
                                reject(new Error(error.message));
                            }
                            else {
                                FS.exists(nameOfTableOfContent, function () {
                                    FS.unlink(nameOfTableOfContent, function () {
                                    });
                                    resolve(mergePdfNames);
                                })
                            }
                        });
                    });
                    }).on('end', function () {
                        tempFile.end();
                    });
            });
        }
        catch (execption) {
            reject(execption.message);
        }
    });
}


/**
* Generating content of  TOC html 
*@param {array} <MetadataList> - <array of links for pdf creation>
* @returns {void} 
*/
let generateTOC = function (MetadataList) {
    for (let dataList in MetadataList) {
        {
            list += "<li>" + MetadataList[dataList].label + "</li>";
            if (MetadataList[dataList].childitems.length > 0) {
                list += "<ul class=\"toc_list\">";
                generateTOC(MetadataList[dataList].childitems);
                list += "</ul>";
            }
        }
    }
    list += "</ul>";
}

/**
* Creates Directory if folder path do not exist 
*@param {array} <outputpath> - <output folder>
* @returns {void} 
*/
let createPdfDir = function (outputpath) {
    if (!FS.existsSync(outputpath)) {
        FS.mkdirSync(outputpath);
    }
}

/**
* Fetching id and url of each topic from Json Object
*@param {array} <MetadataList> - <array of links for pdf creation>
*@param {array} <jsonObject> - <array of links for pdf creation>
* @returns {void} 
* @throws {execption} - execption
*/
let linkObject = function (MetadataList, jsonObject) {
    MetadataList.forEach(item => {
        if (item.link != undefined) {
            if ((item.link.indexOf('.html#') > -1) || (item.link.indexOf('.htm#') > -1)) {
                item.link = item.link.split('#')[0];
            }
            let object = jsonObject.find(function (links) { return links.link == item.link });
            if (object == null) {
                if (item.childitems.length == 0) {
                    jsonObject.push({ id: item.id, parentId: item.parentId, link: item.link, label: item.label });
                }
                else {
                    jsonObject.push({ id: item.id, parentId: item.parentId, link: item.link, label: item.label });
                    return linkObject(item.childitems, jsonObject);
                }
            }
        }
    });
}

/**
* Sorting Main Topics on basis of there ID's
*@param {array} <element> - <json object passed from index.js>
* @returns {void} 
* @throws {execption} - execption
*/
let complexJsonIteration = function (element) {
    if (element.MetadataList != undefined) {
        element.MetadataList =
            element.MetadataList.sort((firstElement, lastElement) => (firstElement.id > lastElement.id) ? 1 : ((lastElement.id > firstElement.id) ? -1 : 0));
    }
    let directoryName = element.documentInfo.document_path;
    const JSON_OBJECT = [];
    linkObject(element.MetadataList, JSON_OBJECT);
    htmlFileCheck(JSON_OBJECT, directoryName, arrayOfLinks);

}

/**
* Pushing html links present in json object to array 
*@param {array} <metadatalistObject> - <json array for metadatalist>
*@param {string} <directoryName> - <name of input directory where html folde is present>
*@param {array} <arrayOfLinks> - <array containing all links>
* @returns {void} 
* @throws {execption} - execption
*/
let htmlFileCheck = function (metadatalistObject, directoryName, arrayOfLinks) {
    metadatalistObject.forEach(function (item) {
        if (item.link != undefined) {
            if ((item.link.indexOf(config.htmlExtension) > -1) || (item.link.indexOf(config.htmExtension) > -1)) {
                item.link = directoryName + item.link;
                arrayOfLinks.push(item.link);
            }
            else {
                console.warn(item.link + config.notValidUrl);
            }
        }
    })

}

/**
* Calling promise of modifyPdf for PDF genration
* @param {string} <jsonObject> - <json Object for json>
*@param {string} <outputPath> - <pdf output folder>
*@param {string} <outPutPdf> - <pdf file name>
*@param {string} <footer> - <footer path>
* @returns {Promise} 
* @throws {execption} - execption
*/
function convertPdf(jsonObject, outputPath, outPutPdf, footer) {
    return new Promise(function (resolve, reject) {
        modifyPdf(jsonObject, outputPath, outPutPdf, footer)
            .then(function (response) {
                try {
                    if (response.split(',')[1] != "") {
                        let fileNames = response.slice(0, response.length - 1);
                        let outputFolder = fileNames.split(',')[0];
                        let files = fileNames.split(',').splice(1, fileNames.split(',').length);
                        let fileName = files[0].split('/')[files[0].split('/').length - 1].split('_')[0] + ".pdf";
                        let clear = setInterval(function () {
                            let array = files;
                            let mergeTrue = [];
                            for (let take of array) {
                                if (FS.existsSync(take)) {
                                    mergeTrue.push('true');
                                }
                                else {
                                    mergeTrue.push('false');
                                }
                            }
                            if (mergeTrue.indexOf('false') == -1) {
                                let object = array;
                                setTimeout(() => {
                                    MERGE(array, outputFolder + "/" + fileName, function (error) {
                                        if (error) {
                                            FS.readdirSync(outputFolder).forEach(file => {
                                                if (file.startsWith(fileName)) {
                                                    FS.unlinkSync(outputFolder + "/"+ + file);
                                                }
                                            })
                                            reject(error.message);
                                        }
                                        for (let file of object) {
                                            FS.exists(file, function () { FS.unlink(file, function () { }); })
                                        }
                                        resolve("Success");
                                    });
                                }, 2000);
                                clearInterval(clear);
                            }
                        }, 3000);
                    }
                    else {
                        resolve("Success");
                    }
                }
                catch (execption) {
                    reject(execption.message);
                    throw execption;
                }
            }).catch(function (error) {
                reject(error);
            });
    });
}

/**
* Calling promise of genratePdf for PDF genration
* @param {string} <jsonObject> - <json Object for json>
*@param {string} <outputPath> - <pdf output folder>
*@param {string} <outPutPdf> - <pdf file name>
*@param {string} <footer> - <footer path>
* @returns {Promise} 
*/
function modifyPdf(jsonObject, outputPath, outPutPdf, footer) {
    return genratePdf(jsonObject, outputPath, outPutPdf, footer);
}

module.exports = convertPdf;